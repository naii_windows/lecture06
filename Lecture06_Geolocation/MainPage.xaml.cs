﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lecture06_Geolocation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            MCMap.MapServiceToken = "NggniNfkWoAJYWaNrwu7~Ba_YkJv9SvsASrBV280AGQ~AqXz_H8d1GdioVSul1nTHcxPtsQlG3YdjJtPP9csVnPPyDNmc7kUv0G8x-QpQueG";

            // Specify a known location.
            BasicGeoposition snPosition = 
                new BasicGeoposition() { Latitude = 51.030460, Longitude = 3.703510 };
            Geopoint snPoint = new Geopoint(snPosition);

            AddPointToMap(snPoint, "Hogent Campus Schoonmeersen");
            AddPOI2();

            // Center the map over one POI.
            MCMap.Center = snPoint;
            MCMap.ZoomLevel = 10;
        }

        private async void AddPOI2()
        {
            BasicGeoposition bg2 = new BasicGeoposition();
            Geopoint gp2 = new Geopoint(bg2);

            MapLocationFinderResult res =
                await MapLocationFinder.FindLocationsAsync("HoGent Campus Aalst, Belgium", gp2);

            MapLocation location = res.Locations.First();

            AddPointToMap(new Geopoint(new BasicGeoposition()
            {
                Longitude = location.Point.Position.Longitude,
                Latitude = location.Point.Position.Latitude
            })
                , "HoGent Campus Aalst");

        }

        private void AddPointToMap(Geopoint geoPoint, string title)
        {
            MapIcon mapIcon = new MapIcon();
            mapIcon.Location = geoPoint;

            mapIcon.NormalizedAnchorPoint = new Point(0.5, 1.0);
            mapIcon.Title = title;
            mapIcon.ZIndex = 0;

            MCMap.MapElements.Add(mapIcon);
        }
    }
}
